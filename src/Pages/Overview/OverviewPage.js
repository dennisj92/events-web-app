import React from 'react'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

import EmptyOverview from './EmptyOverview'
import EventCard from '../../components/Cards/EventCard/EventCard'
import CreateEventCard from '../../components/Cards/UserActionCard/UserActionCard'
import './Overview.css'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}))

const events = [
  {
    name: 'Meetup event',
    date: '2019/08/02',
    startTime: '19:00',
    details: 'Bring stuff and other stuff.',
  },
  {
    name: 'Between Worlds Gank Fiesta',
    date: '2019/08/06',
    startTime: '19:00',
    details: 'Bring stuff and other stuff.',
  },
  {
    name: 'World Boss Run',
    date: '2019/11/23',
    startTime: '09:00',
    details: 'Healers and Tanks needed',
  },
  {
    name: 'Game Development Workshop',
    date: '2020/01/21',
    startTime: '11:30',
    details:
      'Bring your own laptops and food. Basic programming knowledge needed',
  },
  {
    name: 'Meetup event',
    date: '2019/08/02',
    startTime: '19:00',
    details: 'Bring stuff and other stuff.',
  },
  {
    name: 'World Boss Run',
    date: '2019/11/23',
    startTime: '09:00',
    details: 'Healers and Tanks needed',
  },
  {
    name: 'Game Development Workshop',
    date: '2020/01/21',
    startTime: '11:30',
    details:
      'Bring your own laptops and food. Basic programming knowledge needed',
  },
  {
    name: 'Meetup event',
    date: '2019/08/02',
    startTime: '19:00',
    details: 'Bring stuff and other stuff.',
  },
  {
    name: 'World Boss Run',
    date: '2019/11/23',
    startTime: '09:00',
    details: 'Healers and Tanks needed',
  },
  {
    name: 'Game Development Workshop',
    date: '2020/01/21',
    startTime: '11:30',
    details:
      'Bring your own laptops and food. Basic programming knowledge needed',
  },
  {
    name: 'Meetup event',
    date: '2019/08/02',
    startTime: '19:00',
    details: 'Bring stuff and other stuff.',
  },
  {
    name: 'World Boss Run',
    date: '2019/11/23',
    startTime: '09:00',
    details: 'Healers and Tanks needed',
  },
  {
    name: 'Game Development Workshop',
    date: '2020/01/21',
    startTime: '11:30',
    details:
      'Bring your own laptops and food. Basic programming knowledge needed',
  },
]

const OverviewPage = () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      {events.length > 0 ? (
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="stretch"
          spacing={8}
        >
          <Grid item xs={3} className="create-event-grid-item">
            <CreateEventCard />
          </Grid>
          {events.map((event, i) => {
            return (
              <Grid item key={i}>
                <EventCard key={i} event={event} />
              </Grid>
            )
          })}
        </Grid>
      ) : (
        <EmptyOverview />
      )}
    </div>
  )
}

export default OverviewPage
