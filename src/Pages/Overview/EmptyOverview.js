import React from 'react'
import Box from '@material-ui/core/Box';
import { Typography } from '@material-ui/core';

import UserActions from '../../components/Cards/UserActionCard/UserActionCard'

import "./EmptyOverview.css"

const username = "Blargh"

const EmptyOverview = () => {
  return (
    <Box 
      width="40%" 
      textAlign="left" 
      fontWeight="fontWeightRegular"
      className="empty-overview-wrapper"
    >
      <Typography variant="paragraph" component="p">
        Hello {username}. If you already have an event link or ID you can paste that into the menu that pops up when
        you press the "Add Event" button.
      </Typography>
      
      <Typography variant="paragraph" component="p">
      Feeling like organizing an event? Then press the "Create Event" button and share the event with the intended people.
        You can choose between hosting a private or public event.
      </Typography>
      <UserActions />
    </Box>
  )
}

export default EmptyOverview;