import React from 'react'
import { Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import GamesIcon from '@material-ui/icons/Games'
import VideogameIcon from '@material-ui/icons/VideogameAsset'
import EventIcon from '@material-ui/icons/EventAvailable'
import { AppBar } from '@material-ui/core'
import SignIn from '@material-ui/icons/SubdirectoryArrowRight'

import * as ROUTES from '../../constants/routes'

const useStyles = makeStyles(theme => ({
  cover: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    paddingTop: '10%',
    backgroundColor: '#757de8',
  },
  root: {
    padding: theme.spacing(3, 2),
  },
  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: '30px',
  },
  icon: {
    width: '60px',
    height: '60px',
    color: '#fff',
    opacity: '0.7',
  },
  appBar: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  signOut: {
    color: '#fff',
  },
  button: {
    padding: theme.spacing(1),
    color: '#fff',
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
}))

const SignUp = () => {
  const classes = useStyles()

  return (
    <div className={classes.cover}>
      <AppBar className={classes.appBar}>
        <Button variant="text" color="primary" className={classes.button}>
          <Link to={ROUTES.SIGN_IN}>Sign In</Link>
          <SignIn className={classes.rightIcon} />
        </Button>
      </AppBar>
      <Container maxWidth="sm">
        <div className={classes.iconWrapper}>
          <GamesIcon className={classes.icon} />
          <EventIcon className={classes.icon} />
          <VideogameIcon className={classes.icon} />
        </div>
        <Card className={classes.root}>
          <CardActionArea>
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                Create Account
              </Typography>
              <provider />
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button size="small" color="primary">
              Sign Up
            </Button>
            <Button size="small" color="primary">
              Terms
            </Button>
          </CardActions>
        </Card>
      </Container>
    </div>
  )
}

export default SignUp
