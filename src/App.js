import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './App.css'

import Header from './components/Header/Header'
import SignupPage from './Pages/SignUp/SignUp'
import OverviewPage from './Pages/Overview/OverviewPage'

import * as ROUTES from './constants/routes'

function App() {
  const isLoggedIn = true
  return (
    <React.StrictMode>
      <Router>
        <div className="App">{isLoggedIn ? <Header /> : <SignupPage />}</div>
        <Route path={ROUTES.SIGN_IN} component={Header} />
        <Route path={ROUTES.OVERVIEW} component={OverviewPage} />
      </Router>
    </React.StrictMode>
  )
}

export default App
