import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import { Typography } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  menuButton: {
    minWidth: '160px',
    color: '#fff',
    opacity: '0.7',
  },
  bigAvatar: {
    margin: 10,
    width: 40,
    height: 40,
  },
})

export default function SettingsMenu() {
  const classes = useStyles()
  const [state, setState] = React.useState({
    right: false,
  })

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }

    setState({ ...state, [side]: open })
  }

  const sideList = () => (
    <div className={classes.list}>
      <Typography variant="button" component="h2" gutterBottom align="center">
        Settings
      </Typography>
      <List>
        <ListItem key="0">
          <TextField id="Name" label="Name" type="text" fullWidth required />
        </ListItem>
      </List>
    </div>
  )

  return (
    <Fragment>
      <Button
        className={classes.menuButton}
        onClick={toggleDrawer('right', true)}
      >
        Settings
      </Button>
      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer('right', false)}
      >
        {sideList('right')}
      </Drawer>
    </Fragment>
  )
}
