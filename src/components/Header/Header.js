import React from 'react'
import DesktopMenu from './DesktopMenu'

const Header = () => {
  return <DesktopMenu />
}

export default Header
