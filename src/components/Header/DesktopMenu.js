import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import SignOut from '@material-ui/icons/ExitToApp'
import OverviewPage from '../../Pages/Overview/OverviewPage'
import ProfileMenuButton from './../../components/ProfileMenu/ProfileMenu'
import SettingsMenuButton from './../../components/SettingsMenu/SettingsMenu'

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  tabs: {
    flexGrow: 1,
  },
  appBar: {
    flexDirection: 'row',
  },
  signOut: {
    color: '#fff',
  },
  button: {
    padding: theme.spacing(1),
    color: '#fff',
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
}))

export default function DesktopMenu() {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)

  function handleChange(event, newValue) {
    setValue(newValue)
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <Tabs
          className={classes.tabs}
          value={value}
          onChange={handleChange}
          aria-label="Main menu"
        >
          <Tab label="Overview" {...a11yProps(0)} />
          <ProfileMenuButton {...a11yProps(1)} />
          <SettingsMenuButton {...a11yProps(2)} />
        </Tabs>
        <Button variant="text" color="primary" className={classes.button}>
          Sign Out
          <SignOut className={classes.rightIcon} />
        </Button>
      </AppBar>
      <TabPanel value={0} index={0}>
        <OverviewPage />
      </TabPanel>
    </div>
  )
}
