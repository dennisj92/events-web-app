import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'

function SimpleDialog(props) {
  const { onClose, selectedValue, open } = props

  function handleClose() {
    onClose(selectedValue)
  }

  function handleListItemClick(value) {
    onClose(value)
  }

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle id="simple-dialog-title">
        Select how you want to add an event to your schedule
      </DialogTitle>
      <List>
        <ListItem button onClick={() => handleListItemClick('addAccount')}>
          <ListItemText primary="With ID" />
        </ListItem>
        <ListItem button onClick={() => handleListItemClick('addAccount')}>
          <ListItemText primary="With Link" />
        </ListItem>
        <ListItem button onClick={() => handleListItemClick('addAccount')}>
          <ListItemText primary="With QR Code" />
        </ListItem>
      </List>
    </Dialog>
  )
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
}

export default function SimpleDialogDemo() {
  const [open, setOpen] = React.useState(false)

  function handleClickOpen() {
    setOpen(true)
  }

  const handleClose = value => {
    setOpen(false)
  }

  return (
    <React.Fragment>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        {' '}
        Add Event
      </Button>
      <SimpleDialog open={open} onClose={handleClose} />
    </React.Fragment>
  )
}
