import React from 'react'
import Box from '@material-ui/core/Box'

import CreateEventButton from './EventFormButton'
import AddEventButton from './AddEventButton'

import './UserActionCard.css'

const UserActionCard = () => (
  <Box className="event-card-button-wrapper">
    <AddEventButton />
    <CreateEventButton />
  </Box>
)

export default UserActionCard
