import React from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { FormControl } from '@material-ui/core'
import MenuItem from '@material-ui/core/MenuItem'

import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'

const games = [
  {
    name: 'Guild Wars 2',
  },
  {
    name: 'Between Worlds',
  },
  {
    name: 'Everquest',
  },
  {
    name: 'Worlds of Warcraft',
  },
  {
    name: 'Final Fantasy XIV',
  },
  {
    name: 'Runescape',
  },
  {
    name: 'EVE Online',
  },
  {
    name: 'Monster Hunter World',
  },
]

export default function EventFormButton() {
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
  })

  const handleToggleChange = name => event => {
    setState({ ...state, [name]: event.target.checked })
  }

  const [open, setOpen] = React.useState(false)

  const [values, setValues] = React.useState({
    game: 'Guild Wars 2',
  })

  function handleClickOpen() {
    setOpen(true)
  }

  function handleClose() {
    setOpen(false)
  }

  const handleChange = name => event => {
    setValues({ ...values, [name]: event.target.value })
  }

  return (
    <React.Fragment>
      <Button variant="outlined" onClick={handleClickOpen}>
        Create Event
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create New Event</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create an event please fill in all required fields.
          </DialogContentText>
          <FormControl fullWidth>
            <TextField
              autoFocus
              id="name"
              label="Event Name"
              type="text"
              fullWidth
              required
            />
            <TextField
              id="filled-select-currency"
              select
              label="Select Game"
              onChange={handleChange('game')}
              helperText="Please select your game"
              margin="normal"
              variant="filled"
            >
              {games.map(option => (
                <MenuItem key={option.name} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              id="location"
              label="Location"
              type="text"
              fullWidth
              required
            />
            <TextField
              id="startTime"
              label="Start Time"
              type="time"
              fullWidth
              required
            />
            <TextField id="endTime" label="End Time" type="time" fullWidth />
            <TextField
              id="Description"
              label="Description"
              margin="normal"
              variant="outlined"
              multiline
              fullWidth
            />
          </FormControl>
          <FormControlLabel
            control={
              <Switch
                checked={state.checkedB}
                onChange={handleToggleChange('checkedB')}
                value="checkedB"
                color="primary"
              />
            }
            label="Private Event"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleClose} color="primary">
            Create Event
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
}
