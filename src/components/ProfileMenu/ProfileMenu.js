import React, { Fragment } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import { ListItemText } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import Avatar from '@material-ui/core/Avatar'
import AvatarIcon from '@material-ui/icons/AccountCircle'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  menuButton: {
    minWidth: '160px',
    color: '#fff',
    opacity: '0.7',
  },
  bigAvatar: {
    margin: 10,
    width: 40,
    height: 40,
  },
})

const userInfo = {
  name: 'Dennis Johansson',
}

export default function ProfileMenu() {
  const classes = useStyles()
  const [state, setState] = React.useState({
    right: false,
  })

  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return
    }

    setState({ ...state, [side]: open })
  }

  const sideList = side => (
    <div className={classes.list}>
      <Grid container justify="center" alignItems="center"></Grid>
      <Avatar alt={userInfo.name} className={classes.bigAvatar}>
        <AvatarIcon />
      </Avatar>
      <ListItemText primary={userInfo.name} />
      <Grid />
      <Divider />
      <List>
        <ListItem button key="0">
          <TextField id="Name" label="Name" type="text" fullWidth required />
        </ListItem>
      </List>
    </div>
  )

  return (
    <Fragment>
      <Button
        className={classes.menuButton}
        onClick={toggleDrawer('right', true)}
      >
        Profile
      </Button>
      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer('right', false)}
      >
        {sideList('right')}
      </Drawer>
    </Fragment>
  )
}
